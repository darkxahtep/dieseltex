$(document).ready(function () {
    var questionForm = $("#questionForm");
    questionForm.validate({
        rules: {
            inputEmail: {
                required: true,
                email: true
            },
            inputQuestion: {
                required: true
            },
            inputCaptcha:
            {
                required: true
            }
        }
    });

    $("#submitBtn").click(function () {
        if (questionForm.valid()) {
            var email = $("#inputEmail").val();
            var question = $("#inputQuestion").val();
            var keystring = $("#inputCaptcha").val();
            $.post("/mail", { email: email, question: question, keystring: keystring }, function (result) {
                switch (result) {
                    case "200":
                        alert("Сообщение успешно отправлено");
                        $("#inputEmail, #inputQuestion, #inputCaptcha").val("");
                        break;
                    case "403":
                        alert("Неверно введен код с картинки");
                        $("#inputCaptcha").val("").focus();
                        break;
                    default:
                        alert("Ошибка при отправке сообщения");
                        break;
                }
                $("#imgCaptcha").attr("src","/captcha?"+Math.floor(Math.random()*100000));
            });
        } else {
            alert("Не все поля заполнены верно");
        }
    });
});