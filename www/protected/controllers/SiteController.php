<?php

class SiteController extends SFController
{
    public $layout="layouts/box";

	public static function sendEmail($to, $subject, $message, $headers){
		$headers_array = array(
			'MIME-Version: 1.0',
			'Content-Type: text/plain; charset=UTF-8',
			'Content-Transfer-Encoding: base64',
			'From: dmitry.pavluk@gmail.com'
		);
		$headers_custom= implode("\n\r", $headers_array);
		return mail($to, "=?UTF-8?B?".base64_encode($subject)."?=", base64_encode($message), $headers_custom."\n".$headers);
	}

    public function actionIndex()
    {
        $this->render("index");
    }

    public function actionContacts()
    {
        $this->render("contacts");
    }

	public function actionInfo()
	{
		$this->render("info");
	}

	public function actionCaptcha()
	{
		session_start();

		$captcha = new KCAPTCHA();
		$_SESSION['captcha_keystring'] = $captcha->getKeyString();

	}

	public function action404()
	{
		throw new SFHTTPException(404, "Requested URL not found");
	}



	public function actionMail()
	{
		session_start();
		if(count($_POST)>0){
			if(isset($_SESSION['captcha_keystring']) && $_SESSION['captcha_keystring'] === $_POST['keystring']) {
				$e=$_POST['email'];
				$q=$_POST['question'];
				echo self::sendEmail("example@example.com", "Сообщение с DIESEL-TEX", $q, "Reply-To: $e\n") ?
					200 : 500;
			}
			else {
				echo 403;
			}
		}
		else echo 0;
		unset($_SESSION['captcha_keystring']);
	}
}
