<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Dimon
 * Date: 13.08.13
 * Time: 23:23
 * To change this template use File | Settings | File Templates.
 */

class ServiceController extends SFController{
	public $layout="layouts/box";

	public function actionIndex()
	{
		$path_info=isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : "";
		$path=explode("/", $path_info);
		array_shift($path);
		if (count($path)>1) {
			$pageName=$path[1];
			if (preg_match("/^[a-zA-Z0-9\-]+$/", $pageName))
			{
				$viewName="service/$pageName";
				if (SFTemplate::exists($viewName))
				{
					$this->render($viewName);
					return true;
				}
			}
			throw new SFHTTPException(404, "Service page not found.");
		}
		else $this->render("service");
	}
}