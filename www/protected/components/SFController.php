<?php

class SFController
{
    public $layout;
    protected $template;

    public function render($name, $params=null, $return=false)
    {
        $this->template = new SFTemplate($name);
        $this->template->useLayout($this->layout);
        $this->template->controller=$this;
        return $this->template->render($params, $return);
    }

}
