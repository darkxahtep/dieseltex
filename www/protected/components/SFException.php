<?php

class SFException extends Exception
{
    protected $template_name;

    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        if (empty($this->template_name)) $this->template_name='errors/php';
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        //return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
        $template = new SFTemplate($this->template_name);
        die($template->render(array('e'=>$this), true));
    }
}
