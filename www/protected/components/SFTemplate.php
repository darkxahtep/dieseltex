<?php
class SFTemplate
{
    private $filename;
    private $layouts;
    protected $layout;

    public $parent;
    public $children;
    public $params;
    public $controller;

	public static function getTemplatePath($name)
	{
		return realpath(dirname(__FILE__)."/../views/".$name.".php");
	}

	public static function exists($name)
	{
		return file_exists(self::getTemplatePath($name));
	}

    public function __construct($name){
        $this->filename=self::getTemplatePath($name);
        $this->children=array();
        $this->layouts=array();
        if (!self::exists($name)) throw new SFException("Template ".$this->filename." not found!");
    }

    public function __toString()
    {
        return $this->render();
    }

    public function useLayout($name=null)
    {
        if (!is_null($name)) {
            $this->layout=$name;
        }
    }

    public static function getInstance($name)
    {
        return new SFTemplate($name);
    }

    public function beginLayout($name) {
        $this->layouts[]=$name;
        ob_start();
    }

    public function endLayout() {
        if (count($this->layouts)>0) {
            $name=array_pop($this->layouts);
            $params=$this->params;
            $params['content']=ob_get_clean();
            $this->renderChild($name, $params);
        }
        else throw new SFException("Unexpected layout end");
    }

    public static function staticRender($name, $params=null, $return=false)
    {
        $template=self::getInstance($name);
        return $template->render($params, $return);
    }

    public function renderChild($name, $params=null, $return=false)
    {
        if ($params===null) $params=$this->params;
        $template=self::getInstance($name);
        $this->children[]=$template;
        $template->parent=$this;
        return $template->render($params, $return);
    }

    public function render($params=null, $return=false)
    {
        ob_start();
        if (!is_null($params))
        {
            $this->params=$params;
            foreach ($params as $name=>$value)
            {
                $$name=$value;
            }
        }
        include ($this->filename);
        $content=ob_get_clean();
        if (!is_null($this->layout))
        {
            $params['content']=$content;
            $content=SFTemplate::getInstance($this->layout)->render($params, true);
        }
        if (!$return) echo $content;
        return $content;
    }
}
