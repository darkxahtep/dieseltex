<?php
class SFBNavbar
{
    public $active_class='active';
    protected $content;

    public function getLinkHtml($caption, $link)
    {
        $class=strtolower($link[1])==strtolower(SF::app()->pathManager->cpath) ?
            sprintf(' class="%s"', $this->active_class) : "";
        return sprintf('<li%s><a href="%s"><i class="%s"></i>%s</a></li><li class="divider-vertical"></li>', $class, $link[0], $link[2], $caption);
    }

    public static function getInstance($links, $return=true)
    {
        return new self($links, $return);
    }

    public function __construct($links, $return=true)
    {
        $this->content='<ul class="nav">';
        foreach ($links as $caption=>$link)
        {
            $this->content.=$this->getLinkHtml($caption, $link);
        }
		$this->content.='</ul>';
        if (!$return) $this->render();
    }
    public function render($return=false)
    {
        if (!$return) echo $this->content;
        return $this->content;
    }
    public function __toString(){
        return $this->render(true);
    }

}
