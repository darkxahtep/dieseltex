<?php

class SFPathManager
{
    private $config;
    public $path;
    public $controller;
    public $action;
    public $cpath;

    public function __construct($config)
    {
        $this->config=$config;
    }

    public function executeControllerAction()
    {
        $path_info=isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : "";
        $path=explode("/", $path_info);
        array_shift($path);
        if (count($path)==0) {
            $path=array('index');
        }

        $this->path=$path;
        $dir=$path[0];
        $sub_dir=isset($path[1]) ? $path[1] : false;
        if (isset($this->config[$dir]))
        {
            $dir_info=$this->config[$dir];
            $default_controller=$sub_dir!==false ? $sub_dir : 'index';
            $action=isset($dir_info['action']) ? $dir_info['action'] : $default_controller;
            $this->controller=isset($dir_info['controller']) ? $dir_info['controller'] : false;
            if ($this->controller === false) throw new SFException("Controller can't be null");
            $class=$this->controller."Controller";
            $this->action=$action;
            $this->cpath=$this->controller.":".$this->action;
            $action="action".$action;
            $controller=new $class();
            if (method_exists($controller, $action))
            {
                $controller->$action();
            }
            else throw new SFHTTPException(404, "Invalid action \"{$this->action}\"");
        }
        else throw new SFHTTPException(404, "Invalid path \"/".implode("/", $this->path)."\"");
    }

}
