<?php

class SFHTTPException extends SFException
{
    // Redefine the exception so message isn't optional
    public function __construct($code = 404, $message, Exception $previous = null) {
        $this->template_name='errors/http';
        parent::__construct($message, $code, $previous);
    }
}
