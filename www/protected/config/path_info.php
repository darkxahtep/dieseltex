<?php
return array(
    'index'=>array(
        'controller' => 'Site',
    ),

	'404'=>array(
		'controller' => 'Site',
		'action' => '404',
	),

    'contacts'=>array(
        'controller' => 'Site',
        'action' => 'Contacts',
    ),

	'info'=>array(
		'controller' => 'Site',
		'action' => 'Info',
	),

	'mail'=>array(
		'controller' => 'Site',
		'action' => 'Mail',
	),
	'captcha'=>array(
		'controller' => 'Site',
		'action' => 'Captcha',
	),

	'service'=>array(
		'controller' => 'Service',
		'action' => 'Index',
	),
);