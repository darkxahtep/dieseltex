<?php

class SF
{
    protected static $instance;

    private function __clone()    {  }  // Защищаем от создания через клонирование
    private function __wakeup()   {  }  // Защищаем от создания через unserialize

    public $pathManager;
    public $cache;
    protected $root;
    public $title;
    protected $config;

    private function __construct(){
        $this->root=realpath(dirname(__FILE__))."/";
    }

    public static function app() {    // Возвращает единственный экземпляр класса. @return SF
        if ( is_null(self::$instance) ) {
            self::$instance = new SF;
        }
        return self::$instance;
    }

    public static function importAll(){
        self::importDir("components/");
        self::importDir("components/external");
        self::importDir("components/exceptions");
        self::importDir("components/widgets");
        self::importDir("models/");
        self::importDir("controllers/");
		require_once("components/kcaptcha/kcaptcha.php");
    }

    public function getRoot($plus=''){
        return $this->root.$plus;
    }

    public static function importDir($path)
    {
        $directory=realpath(self::app()->root.$path);
        if ($directory===false) return false; //TODO: add exeption if not exists
        $dir = opendir($directory);
        while($file = readdir($dir))
        {
            if ( is_file ($directory."/".$file))
            {
                if (file_exists($directory."/".$file))
                {
                    if (preg_match('/.php$/is', $file))
                    {
                        require_once($directory."/".$file);
                    }

                }
            }
        }
        return true;
    }

    private function _configure() {
        $this->title=$this->config['title'];
    }

    public function configure($config) //  @return SF
    {
        if (is_array($config)) {
            $this->config=$config;
        }
        else throw new SFException("SF::Configure - \$config must be an array.");
        $this->_configure();
        return $this;
    }

    protected function init() {
        $path_config=include(realpath($this->root."config/path_info.php"));
        $this->pathManager=new SFPathManager($path_config);
    }

    public function run(){ //  @return SF
        $this->init();
        $this->pathManager->executeControllerAction();
        return $this;
    }
}
