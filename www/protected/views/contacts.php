<?php
SF::app()->title.=" - Контакты";
?>
<h2 class="text-center">Контакты</h2>
<p>Вы можете связаться с нами следующими способами:</p>
<ul class="list list-contacts">
	<li>Электронная почта<br>
		<em>diesel-tex@ya.ru</em>
	</li>
	<li>Телефон<br>
		<em>+38-096-207-5508<br />
			+38-093-921-9488<br />
			(Валерий)</em>
	</li>
	<li>Адрес<br>
		<em>город Харьков, улица ....., дом ....</em>
	</li>
</ul>
<p>
	Также Вы можете задать интересующий Вас вопрос воспользовавшись следующей формой обратной связи:
</p>
<form id="questionForm" class="form-horizontal" action="#" method="post">
	<div class="control-group offset2">
		<label class="control-label" for="inputEmail">Ваш e-mail адрес</label>
		<div class="controls">
			<input type="text" id="inputEmail" name="inputEmail" class="span4">
		</div>
	</div>
	<div class="control-group offset2">
		<label class="control-label" for="inputQuestion">Вопрос</label>
		<div class="controls">
			<textarea id="inputQuestion" name="inputQuestion" rows="4" cols="20" class="span4"></textarea>
		</div>
	</div>
	<div class="control-group offset2">
		<label class="control-label" for="inputCaptcha">Введите код с картинки</label>
		<div class="controls">
			<img src="/captcha" id="imgCaptcha" class="img-rounded" alt="Защитный код"> <br><br>
			<input type="text" id="inputCaptcha" name="inputCaptcha" class="span4">
		</div>
	</div>
	<div class="control-group offset2">
		<div class="controls">
			<button type="button" id="submitBtn" class="btn">Задать вопрос</button>
		</div>
	</div>
</form>

<script src="/scripts/jquery.validate.js" type="text/javascript"></script>
<script src="/scripts/jquery.validate.additional.js" type="text/javascript"></script>
<script src="/scripts/jquery.validate.messages_ru.js" type="text/javascript"></script>
<script src="/scripts/contacts.js" type="text/javascript"></script>
