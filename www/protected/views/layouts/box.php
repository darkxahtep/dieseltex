<?php
$this->beginLayout("layouts/main");
$links = new SFBNavbar(array(
    'Главная' => array("/", "site:index", "icon-home"),
    'Услуги' => array("/service/", "service:index", "icon-wrench"),
    'Контакты' => array("/contacts/", "site:contacts", "icon-envelope"),
    'Полезная информация' => array("/info/", "site:info", "icon-book"),
));

?>

	<div class="row">
		<div class="span12 shadow">
			<img src="/images/header.png" class="header-image" alt="Header" />
		</div>
	</div>
	<div class="row">
		<div class="span12">
			<div class="navbar">
				<div class="navbar-inner  shadow">
					<?php
						echo $links;
					?>
				</div>
			</div>
		</div>
	</div>
	<div class="content row">
		<div class="span12">
			<div class="well shadow">
        <?php echo $content;?>
		</div>
	</div>
</div>

<div class="row">
	<div class="span12">
		<div class="well text-center footer shadow">
			&copy;&nbsp;«DIESEL-TEX» 2013
		</div>
	</div>
</div>
<?php
$this->endLayout();