<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width" />
	<title><?php echo SF::app()->title; ?></title>
	<meta name="description" content="Ремонт дизельной аппаратуры и оборудования в городе Харьков">
	<meta name="keywords" content="ремонт, дизель, diesel, тнвд, common rail, Харьков, топливная аппаратура, распылитель, насос-форсунка, контакты, обратная связь">
	<meta name="author" content="DIESEL-ТЕХ">
	<link href="/styles/bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="/styles/site.css" rel="stylesheet" type="text/css" />
	<script src="/scripts/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="/scripts/bootstrap.js" type="text/javascript"></script>
</head>
<body>
<div class="container">

<?php
    echo $content;
?>

</div>
</body>
</html>