﻿<?php SF::app()->title.=" - RENAULT"; ?>
    <div class="text-center">
        <img src="/images/models-big/renault.png" alt="RENAULT" />
    </div>
    <br />
    <p>
        Renault контролирует корейскую компанию Samsung Motors (80,1 %) и румынскую Dacia (99,43 %),
                        владеет 44,3 % акций японской компании Nissan Motor 30 % компании Daimler и 20,5 % шведской AB Volvo. Альянс Renault и Nissan (Renault Nissan Alliance)
                        занимает четвёртое место в мире по выпуску автомобилей.
    </p>
    <p>
        Компания выпускает автомобили под марками «Renault», «Samsung» и «Dacia». В модельный ряд входят легковые и коммерческие (в том числе тяжёлые грузовики)
                        автомобили, тракторы и т. д.)
    </p>
    <p>
        Renault Trucks — французский производитель грузовиков тяжелого класса.
    </p>
    <p>
        Компания с 2001 года принадлежит Volvo Group. Renault Trucks присутствует более чем в 100 различных странах на 5 континентах. Разработка и сборка автомобилей, а также производство компонентов, осуществляются во Франции и Испании.
    </p>
    <table class="table table-bordered table-striped table-price">
        <thead>
            <tr>
                <th rowspan="2">Тип ТНВД</th>
                <th colspan="2">Итого ремонт ТНВД</th>
                <th colspan="2">Итого ремонт форсунки без замены проставки</th>
            </tr>
            <tr>
                <th>С авто</th>
                <th>Без авто</th>
                <th>С авто</th>
                <th>Без авто</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>6-ка мех.</th>
                <td>7300</td>
                <td>6750</td>
                <td>250</td>
                <td>170</td>
            </tr>
            <tr>
                <th>6-ка элек.</th>
                <td>9000</td>
                <td>8350</td>
                <td>270</td>
                <td>190</td>
            </tr>
        </tbody>
    </table>
    <br />
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Ремонт</th>
                <th>Стоимость</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>PLD-секция (индивидуальный насос) Bosch</td>
                <td>1400 грн./шт</td>
            </tr>
            <tr>
                <td>Форсунка CR, Renault Premium</td>
                <td>1600 грн./шт.</td>
            </tr>
            <tr>
                <td>Форсунка CR, Delphi, Denso, Bosch</td>
                <td>1250 грн./шт.</td>
            </tr>
            <tr>
                <td>Плунжерная пара P-типа</td>
                <td>180 грн./шт.</td>
            </tr>
            <tr>
                <td>Мультипликатор Bosch</td>
                <td>400 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки и PLD-секции</td>
                <td>250 грн./шт.</td>
            </tr>
            <tr>
                <td>Распылитель форсунки</td>
                <td>150 грн./шт.</td>
            </tr>
        </tbody>
    </table>
    <br />
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Продажа</th>
                <th>Стоимость</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Ремкомплект ТНВД 2417010008</td>
                <td>312 грн./шт.</td>
            </tr>
            <tr>
                <td>Ремкомплект ТНВД 2417010010</td>
                <td>253 грн./шт.</td>
            </tr>
            <tr>
                <td>Плунжерная пара P-типа</td>
                <td>180 грн./шт.</td>
            </tr>
            <tr>
                <td>Мультипликатор Bosch</td>
                <td>400 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки и PLD-секции</td>
                <td>250 грн./шт.</td>
            </tr>
            <tr>
                <td>Распылитель форсунки</td>
                <td>150 грн./шт.</td>
            </tr>
            <tr>
                <td>Шайба регулировочная В26-магнитного зазора</td>
                <td>40 грн./шт.</td>
            </tr>
            <tr>
                <td>Шайба регулировочная В22-хода иглы распылителя</td>
                <td>32 грн./шт.</td>
            </tr>
            <tr>
                <td>Шайба регулировочная В14-холостого хода</td>
                <td>32 грн./шт.</td>
            </tr>
            <tr>
                <td>Шайба регулировочная В31-хода шарика</td>
                <td>32 грн./шт.</td>
            </tr>
            <tr>
                <td>Шайба регулировочная В16-Б-хода иглы распылителя</td>
                <td>32 грн./шт.</td>
            </tr>
            <tr>
                <td>Шайба регулировочная В27-магнитного зазора форсунки Denso</td>
                <td>35 грн./шт.</td>
            </tr>
        </tbody>
    </table>
