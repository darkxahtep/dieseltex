<?php SF::app()->title.=" - IFA"; ?>
<div class="text-center">
	<img src="/images/models-big/ifa.png" alt="IFA" />
</div>
<br />
<p>
	Industrieverband Fahrzeugbau (сокр. IFA) — объединение производителей транспортных средств в ГДР.
	По приказу Советской военной администрации в Германии конфискованные предприятия были объединены в 65 промышленных управлениях.
	Из 18 западносаксонских заводов, которые производили транспортные средства, 1 июля 1946 года было образовано «Промышленное управление № 19» (Industrieverwaltung 19 Fahrzeugbau) с штаб-квартирой в Кемнице. 1 июля 1948 года это «IV 19» было расширено на всю территорию Советской зоны оккупации Германии и получило название «IFA Vereinigung Volkseigener Fahrzeugwerke».
	Oбъединению принадлежали такие марки как Trabant, Wartburg, Barkas, Robur, Multicar, Simson и MZ.
</p>
<table class="table table-bordered table-striped table-price">
	<thead>
	<tr>
		<th>Тип ТНВД ИФА</th>
		<th>За 1 насос</th>
		<th>За 1 форсунку</th>
	</tr>
	</thead>
	<tbody>
	<tr>
		<th>обычн.</th>
		<td>1200</td>
		<td>100</td>
	</tr>
	</tbody>
</table>