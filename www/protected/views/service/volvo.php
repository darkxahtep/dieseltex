﻿<?php SF::app()->title.=" - Вольво, Volvo"; ?>
    <div class="text-center">
        <img src="/images/models-big/volvo.png" alt="" />
    </div>
    <br />
    <p>
        Aktiebolaget Volvo (Volvo Group, Volvokoncernen) — шведский концерн. Производит коммерческие и грузовые автомобили, автобусы,
                        двигатели и различное оборудование. Ранее концерн Volvo производил также легковые автомобили, но в 1999 продал своё отделение легковых
                        автомобилей под именем Volvo Personvagnar концерну Ford, который в 2010 перепродал его концерну Geely.
    </p>
    <p>
        В переводе с латыни «volvo» означает «я кручусь» или «я качусь». Штаб-квартира — в городе Гётеборг, Швеция.
    </p>
    <p>
        В настоящее время концерн Volvo является шведским поставщиком грузовиков, автобусов и строительного оборудования, систем морских двигателей,
                        космических компонентов и финансовых услуг.
    </p>
    <table class="table table-bordered table-striped table-price">
        <thead>
            <tr>
                <th rowspan="2">Тип ТНВД</th>
                <th colspan="2">Итого ремонт ТНВД</th>
                <th colspan="2">Итого ремонт форсунки без замены проставки</th>
            </tr>
            <tr>
                <th>С авто</th>
                <th>Без авто</th>
                <th>С авто</th>
                <th>Без авто</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>6-ка насос-форсунка</th>
                <td>1650</td>
                <td>1250</td>
                <td>-</td>
                <td>-</td>
            </tr>
        </tbody>
    </table>
    <br />
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Ремонт</th>
                <th>Стоимость</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Насос-форсунка Volvo, Lucas - Mark1, Mark2 Насос-форсунка Bosch</td>
                <td>1550 грн./шт</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки в сборе Mark1, Mark2</td>
                <td>400 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки и PLD-секции</td>
                <td>250 грн./шт.</td>
            </tr>
            <tr>
                <td>Распылитель форсунки</td>
                <td>150 грн./шт.</td>
            </tr>
        </tbody>
    </table>
    <br />
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Продажа</th>
                <th>Стоимость</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Мультипликатор Bosch</td>
                <td>400 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки в сборе Mark1, Mark2</td>
                <td>400 грн./шт.</td>
            </tr>
            <tr>
                <td>Распылитель форсунки</td>
                <td>150 грн./шт.</td>
            </tr>
        </tbody>
    </table>