﻿<?php SF::app()->title.=" - Mercedes"; ?>
    <div class="text-center">
        <img src="/images/models-big/mercedes.png" alt="Mercedes" />
    </div>
    <br />
    <p>
        На данный момент Мерседес предлагает следующую гамму грузовых автомобилей:
    </p>
    <ul class="list">
        <li>Mерседес-Бенц Атего (Mercedes-Benz Atego) — популярное в Европе семейство среднетоннажных грузовых автомобилей
                            полной массой от 6.5 до 16 тонн.</li>
        <li>Mercedes-Benz Axor (рус. Мерседес-Бенц Аксор) — семейство тяжёлых грузовых автомобилей полной массой от 18 до 26 тонн.</li>
        <li>Мерседес-Бенц Актрос (Mercedes-Benz Actros) — семейство тяжелых грузовиков и седельных тягачей полной массой от 18 до 25 тонн.</li>
        <li>Унимог (нем. Mercedes-Benz Unimog, от нем. Universalmotorgerat — универсальное транспортное устройство) —
                            семейство универсальных немецких грузовиков-вездеходов для специального использования
                            (имеется широкая гамма дополнительного навесного оборудования) и транспортировки в экстремальных условиях.</li>
        <li>Mercedes-Benz Zetros.Грузовик от Mercedes-Benz для работы в условиях экстремального бездорожья.
                            Отличается капотной компоновкой, что облегчает ремонт и обеспечивает лучшую развесовку.</li>
    </ul>
    <table class="table table-bordered table-striped table-price">
        <thead>
            <tr>
                <th rowspan="2">Тип ТНВД</th>
                <th colspan="2">Итого ремонт ТНВД</th>
                <th colspan="2">Итого ремонт форсунки без замены проставки</th>
            </tr>
            <tr>
                <th>С авто</th>
                <th>Без авто</th>
                <th>С авто</th>
                <th>Без авто</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>4-ка мех.</th>
                <td>3600</td>
                <td>3150</td>
                <td>250</td>
                <td>170</td>
            </tr>
            <tr>
                <th>6-ка мех.</th>
                <td>7300</td>
                <td>6750</td>
                <td>250</td>
                <td>170</td>
            </tr>
            <tr>
                <th>6-ка элек.</th>
                <td>10400</td>
                <td>9650</td>
                <td>270</td>
                <td>190</td>
            </tr>
        </tbody>
    </table>
    <br />
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Ремонт</th>
                <th>Стоимость</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>PLD-секция (индивидуальный насос) Bosch</td>
                <td>1400 грн./шт.</td>
            </tr>
            <tr>
                <td>Плунжерная пара H-типа</td>
                <td>430 грн./шт</td>
            </tr>
            <tr>
                <td>Форсунка CR, Delphi, Denso, Bosch</td>
                <td>1250 грн./шт.</td>
            </tr>
            <tr>
                <td>Плунжерная пара P-типа</td>
                <td>180 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки и PLD-секции</td>
                <td>250 грн./шт.</td>
            </tr>
            <tr>
                <td>Распылитель форсунки</td>
                <td>150 грн./шт.</td>
            </tr>
        </tbody>
    </table>
    <br />
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Продажа</th>
                <th>Стоимость</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Плунжерная пара H-типа</td>
                <td>430 грн./шт</td>
            </tr>
            <tr>
                <td>Ремкомплект ТНВД 2417010004</td>
                <td>365 грн./шт.</td>
            </tr>
            <tr>
                <td>Ремкомплект ТНВД 2417010008</td>
                <td>312 грн./шт.</td>
            </tr>
            <tr>
                <td>Ремкомплект ТНВД 2417010010</td>
                <td>253 грн./шт.</td>
            </tr>
            <tr>
                <td>Плунжерная пара P-типа</td>
                <td>180 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки и PLD-секции</td>
                <td>250 грн./шт.</td>
            </tr>
            <tr>
                <td>Распылитель форсунки</td>
                <td>150 грн./шт.</td>
            </tr>
        </tbody>
    </table>
