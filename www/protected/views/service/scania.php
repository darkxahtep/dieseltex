﻿<?php SF::app()->title.=" - SCANIA"; ?>
    <div class="text-center">
        <img src="/images/models-big/scania.png" alt="SCANIA" />
    </div>
    <br />
    <p>
        Scania AB — крупнейший шведский производитель автобусов и грузовых автомобилей, выпускает автобусы с 1920 года.
                        Штаб-квартира располагается в городе Сёдертелье.
    </p>
    <p>Крупнейшие акционеры на октябрь 2011 года: Volkswagen AG (70,94 %), MAN (17,37 %).</p>
    <p>
        Scania производит грузовые автомобили, автобусы, судовые и дизельные двигатели. Также компания поставляет свои шасси многим сторонним кузовным предприятиям.
                        Наиболее удачное сотрудничество у Scania Bus наметилось с испанской фирмой Irizar.В 2005 Scania выпустила 52 567 грузовиков и 5816 автобусов.
                        Выручка за этот год составила $7,97 млрд, чистая прибыль — $587 млн.В 2004 и 2009 годах грузовые автомобили Scania серии R удостаивались престижной
                        международной премии «Грузовик года».
    </p>
    <table class="table table-bordered table-striped table-price">
        <thead>
            <tr>
                <th rowspan="2">Тип ТНВД</th>
                <th colspan="2">Итого ремонт ТНВД</th>
                <th colspan="2">Итого ремонт форсунки без замены проставки</th>
            </tr>
            <tr>
                <th>С авто</th>
                <th>Без авто</th>
                <th>С авто</th>
                <th>Без авто</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>6-ка мех.</th>
                <td>7300</td>
                <td>6750</td>
                <td>250</td>
                <td>170</td>
            </tr>
            <tr>
                <th>8-ка мех.</th>
                <td>8900</td>
                <td>8200</td>
                <td>250</td>
                <td>170</td>
            </tr>
            <tr>
                <th>8-ка элек.</th>
                <td>10400</td>
                <td>9650</td>
                <td>270</td>
                <td>190</td>
            </tr>
        </tbody>
    </table>
    <br />
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Ремонт</th>
                <th>Стоимость</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Форсунка CR, Delphi, Denso, Bosch</td>
                <td>1250 грн./шт.</td>
            </tr>
            <tr>
                <td>Плунжерная пара P-типа</td>
                <td>180 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки и PLD-секции</td>
                <td>250 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки в сборе Mark1, Mark2</td>
                <td>400 грн./шт.</td>
            </tr>
            <tr>
                <td>Распылитель форсунки</td>
                <td>150 грн./шт.</td>
            </tr>
        </tbody>
    </table>
    <br />
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Продажа</th>
                <th>Стоимость</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Ремкомплект ТНВД 2417010008</td>
                <td>312 грн./шт.</td>
            </tr>
            <tr>
                <td>Ремкомплект ТНВД 2417010010</td>
                <td>253 грн./шт.</td>
            </tr>
            <tr>
                <td>Плунжерная пара P-типа</td>
                <td>180 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки и PLD-секции</td>
                <td>250 грн./шт.</td>
            </tr>
            <tr>
                <td>Клапан насос-форсунки в сборе Mark1, Mark2</td>
                <td>400 грн./шт.</td>
            </tr>
            <tr>
                <td>Распылитель форсунки</td>
                <td>150 грн./шт.</td>
            </tr>
        </tbody>
    </table>
