<?php $this->beginLayout("layouts/exception"); ?>
<h1>HTTP Error <?php echo $e->getCode(); ?></h1>
    <p class="error_msg">
        <?php echo $e->getMessage(); ?>
    </p>
<?php $this->endLayout(); ?>