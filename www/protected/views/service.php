<?php
SF::app()->title.=" - Услуги";
?>

<img src="/images/check-list.png" class="article-image" style="float: right" alt="Список" />
<h2 class="text-center">Услуги</h2>
<p>В данном разделе вы сможете найти информацию о предоставляемых компанией услугах по ремонту, продаже, обслуживанию топливных систем</p>
<p>Компания «DIESEL-TEX» проводит следующие виды работ:</p>
<ul class="list">
	<li>ремонт тнвд</li>
	<li>ремонт дизельной аппаратуры</li>
	<li>ремонт топливной аппаратуры</li>
	<li>ремонт насос-форсунок</li>
	<li>ремонт распылителя</li>
	<li>ремонт плунжерной пары</li>
	<li>ремонт Common Rail</li>
</ul>
<div class="row-fluid">
	<div class="span4">
		<ul class="list-models">
			<li><a href="/service/man">
					<img src="/images/models/man.png" class="logo-model" alt="лого" />МАН</a></li>
			<li><a href="/service/daf">
					<img src="/images/models/daf.png" class="logo-model" alt="лого" />ДАФ</a></li>
			<li><a href="/service/renault">
					<img src="/images/models/renault.png" class="logo-model" alt="лого" />РЕНО</a></li>
			<li><a href="/service/mercedes">
					<img src="/images/models/mercedes.png" class="logo-model" alt="лого" />МЕРСЕДЕС</a></li>
			<li><a href="/service/iveco">
					<img src="/images/models/iveco.png" class="logo-model" alt="лого" />ИВЕКО</a></li>
		</ul>
	</div>
	<div class="span4">
		<ul class="list-models">
			<li><a href="/service/scania">
					<img src="/images/models/scania.png" class="logo-model" alt="лого" />СКАНИА</a></li>
			<li><a href="/service/volvo">
					<img src="/images/models/volvo.png" class="logo-model" alt="лого" />ВОЛЬВО</a></li>
			<li><a href="/service/maz">
					<img src="/images/models/maz.png" class="logo-model" alt="лого" />МАЗ</a></li>
			<li><a href="/service/kamaz">
					<img src="/images/models/kamaz.png" class="logo-model" alt="лого" />КАМАЗ</a></li>
			<li><a href="/service/tatra">
					<img src="/images/models/tatra.png" class="logo-model" alt="лого" />ТАТРА</a></li>
		</ul>
	</div>
	<div class="span4">
		<ul class="list-models">
			<li><a href="/service/mtz">
					<img src="/images/models/mtz.png" class="logo-model" alt="лого" />МТЗ</a></li>
			<li><a href="/service/balcancar">
					<img src="/images/models/bal.png" class="logo-model" alt="лого" />БАЛКАНКАР</a></li>
			<li><a href="/service/ikarus">
					<img src="/images/models/ikarus.png" class="logo-model" alt="лого" />ИКАРУС</a></li>
			<li><a href="/service/ifa">
					<img src="/images/models/ifa.png" class="logo-model" alt="лого" />ИФА</a></li>
			<li><a href="/service/micro">
					<img src="/images/models/micro.png" class="logo-model" alt="лого" />Микроавтобусы, легковые</a></li>
		</ul>
	</div>
<p>Для уточнения возможности ремонта оборудования не вошедшего в список обращайтесь по приведенным в соответствующем разделе контактам</p>

</div>